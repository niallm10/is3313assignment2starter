/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simonwoodworth.javamavencalcinclassdemo;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author simon
 */
public class CalculatorTest {
    
   @Test
    public void testAdd() {
        Calculator c = new Calculator();
        assertEquals(12,c.add(7, 5));
    }
    
    @Test
    public void testMultiply() {
        Calculator c = new Calculator();
        assertEquals(35,c.multiply(7, 5));
    }
    
    @Test
    public void testSubtract() {
        Calculator c = new Calculator();
        assertEquals(2,c.subtract(7, 5));
    }
    
    @Test
    public void testDivide() {
        Calculator c = new Calculator();
        assertEquals(1,c.divide(7, 5));
    }
    
     @Test
    public void testSquare() {
        Calculator c = new Calculator();
        int expected = 8*8;
        assertEquals(expected,c.square(8));
    }

    @Test
    public void testCube() {
        Calculator c = new Calculator();
        int expected = 3*3*3;
        assertEquals(expected,c.cube(3));
    }

    @Test
    public void testModulo() {
        Calculator c = new Calculator();
        int expected = 14 % 5;
        assertEquals(expected,c.modulo(14,5));
    }

    @Test
    public void testAddThree () {
        Calculator c = new Calculator();
        int expected = 10 + 5 + 2;
        assertEquals(expected,c.addThree(10, 5, 2));
    }
    
    @Test
    public void testSubtractThree () {
        Calculator c = new Calculator();
        int expected = 10 - 5 - 2;
        assertEquals(expected,c.subtractThree(10, 5, 2));
    }
    
    @Test
    public void testMultiplyThree () {
        Calculator c = new Calculator();
        int expected = 10 * 5 * 2;
        assertEquals(expected,c.multiplyThree(10, 5, 2));   
    }
    
    @Test
    public void testDivideThree () {
        Calculator c = new Calculator();
        int expected = 10 / 5 / 2;
        assertEquals(expected,c.divideThree(10, 5, 2));  
    }

}
